package com.atlassianlabs.perftools.collectors.diskspeed;

/**
 * Because Disk Speed is fairly deeply nested object, we can't easily convert it to JSON when using mockito mocks, so we stub it.
 */
public class StubDiskSpeedTimerList extends DiskSpeedTimerList {
    @Override
    public String getAverage() {
        return "16,523";
    }

    @Override
    public String getMedian() {
        return "27,770";
    }

    @Override
    public String getMax() {
        return "902,019";
    }

    @Override
    public String getMin() {
        return "40,738";
    }
}