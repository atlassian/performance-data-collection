package com.atlassianlabs.perftools.collectors.threaddump;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.virtualmachines.AutoCloseableHotSpotVm;
import com.atlassianlabs.perftools.virtualmachines.VirtualMachineFactory;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JCmdThreadDumpTest extends AbstractBaseTest {
    private static final String DUMP = "this chicken is delicious";
    private static final long PID = 1984L;

    @Mock
    private VirtualMachineFactory virtualMachineFactory;
    @Mock
    private AutoCloseableHotSpotVm hotSpotVm;

    @InjectMocks
    private JCmdThreadDump jCmdThreadDump;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        
        final InputStream in = IOUtils.toInputStream(DUMP, "UTF-8");

        when(virtualMachineFactory.getHotSpotVirtualMachine(PID)).thenReturn(hotSpotVm);
        when(hotSpotVm.executeJCmd("Thread.print")).thenReturn(in);
    }

    @Test
    public void doesJCmdWorkAsExpected() throws Exception {
        assertThat(jCmdThreadDump.collectFromProcess(PID), is(DUMP));
    }

    @Test
    public void doWeDetachVmWhenDone() throws Exception {
        jCmdThreadDump.collectFromProcess(PID);

        verify(hotSpotVm).close();
    }

    @Test(expected = NoSuchMethodException.class)
    public void itShouldNotRethrowException() throws Exception {
        when(virtualMachineFactory.getHotSpotVirtualMachine(PID)).thenThrow(new NoSuchMethodException("GAME OVER MAN, WE'RE SCREWED"));

        jCmdThreadDump.collectFromProcess(PID);
    }
}