package com.atlassianlabs.perftools.converters;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.http.MockHttpOutputMessage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;

public class ResponseToTxtFileHttpConverterTest {
    private MockHttpOutputMessage message;
    private ResponseToTxtFileHttpConverter converter;

    @Before
    public void setUp() throws Exception {
        message = new MockHttpOutputMessage();
        converter = new ResponseToTxtFileHttpConverter();
    }

    @Test
    public void itShouldSetPotatoAsAttachment() throws Exception {
        converter.writeInternal("potato", message);

        assertThat(message.getHeaders().getContentType(), equalTo(APPLICATION_OCTET_STREAM));
        assertThat(message.getHeaders().get("Content-Disposition").toString(), containsString("attachment; filename=results_"));
        assertThat(message.getBody().toString(), containsString("potato"));
    }

    @Test
    public void itShouldOnlySupportOctetStream() throws Exception {
        assertThat(converter.getSupportedMediaTypes(), hasSize(1));
        assertThat(converter.getSupportedMediaTypes(), hasItem(APPLICATION_OCTET_STREAM));
    }
}