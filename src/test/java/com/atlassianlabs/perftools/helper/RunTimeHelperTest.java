package com.atlassianlabs.perftools.helper;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import static java.lang.System.nanoTime;
import static java.util.Optional.empty;
import static org.apache.commons.lang3.SystemUtils.IS_OS_UNIX;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assume.assumeThat;

public class RunTimeHelperTest {
    @Before
    public void setUp() throws Exception {
        assumeThat(IS_OS_UNIX, equalTo(true));
    }

    @Test
    public void itShouldSpawnEchoProcess() throws Exception {
        final Process proc = new RunTimeHelper().spawnProcessSafely("echo", "OK")
                .orElseThrow(RuntimeException::new);

        assertThat(IOUtils.toString(proc.getInputStream()), equalTo("OK\n"));
    }

    @Test
    public void itShouldReturnEmptyIfProcessIsNotSpawnable() throws Exception {
        assertThat(new RunTimeHelper().spawnProcessSafely("_notexistingcommand_" + nanoTime()),
                equalTo(empty()));
    }
}