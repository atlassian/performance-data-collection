package com.atlassianlabs.perftools.helper;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static com.atlassianlabs.perftools.helper.MapHelper.asMap;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MapHelperTest {
    @Test
    public void verifyMapHelperWorks() {
        Map<String, Object> map = asMap(
                "long", 1L,
                "map", asMap("float", 1.0f)
        );

        assertThat(map.get("long"), is(1L));
        assertThat(((Map) map.get("map")).get("float"), is(1.0f));
        assertThat(map, is(instanceOf(ImmutableMap.class)));
    }
}