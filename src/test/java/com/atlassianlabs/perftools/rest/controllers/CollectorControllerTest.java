package com.atlassianlabs.perftools.rest.controllers;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.collectors.diskspeed.DiskSpeedCollector;
import com.atlassianlabs.perftools.collectors.diskspeed.DiskSpeedOperation;
import com.atlassianlabs.perftools.collectors.diskspeed.StubDiskSpeedTimerList;
import com.atlassianlabs.perftools.collectors.results.ComboResult;
import com.atlassianlabs.perftools.collectors.results.CpuUsageResult;
import com.atlassianlabs.perftools.collectors.results.DiskSpeedResult;
import com.atlassianlabs.perftools.collectors.results.ThreadDumpResult;
import com.atlassianlabs.perftools.rest.exceptions.NotFoundException;
import com.atlassianlabs.perftools.services.ComboService;
import com.atlassianlabs.perftools.services.CpuUsageService;
import com.atlassianlabs.perftools.services.ThreadDumpService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.io.IOException;

import static com.atlassianlabs.perftools.JsonConverter.convertObjectToJson;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.TEXT_PLAIN;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class CollectorControllerTest extends AbstractBaseTest {
    private static final long PID = 4242L;
    private static final String DIR = "/kanye/west/is/a/lyrical/genius";
    private static final String COLLECTOR_NAME = "SHOW ME WHAT YOU GOT";

    private static final String COLLECT_DISKSPEED_PATH = "/rest/collect/diskspeed";
    private static final String COLLECT_CPU_PATH = "/rest/collect/cpu/";
    private static final String COLLECT_COMBO_PATH = "/rest/collect/combo/";
    private static final String COLLECT_TDUMP_PATH = "/rest/collect/threaddump/";

    @MockBean
    private ThreadDumpService threadDumpService;
    @MockBean
    private DiskSpeedCollector diskSpeedCollector;
    @MockBean
    private CpuUsageService cpuUsageService;
    @MockBean
    private ComboService comboService;

    @Autowired
    private MockMvc mvc;

    private ComboResult comboResult;
    private DiskSpeedResult diskSpeedResult;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        // Need to use stubs so we can serialise it into JSON.
        final DiskSpeedOperation speedOperation = new DiskSpeedOperation("open", null);
        speedOperation.setDiskSpeedTimerList(new StubDiskSpeedTimerList());
        diskSpeedResult = new DiskSpeedResult(DIR,
                singletonList(speedOperation));

        comboResult = new ComboResult(singletonList(successfulCpuUsageResult), singletonList(successfulThreadDumpResult));

        when(diskSpeedCollector.collectDiskSpeed(eq(DIR), anyInt())).thenReturn(diskSpeedResult);
        when(threadDumpService.collectFromPid(PID)).thenReturn(successfulThreadDumpResult);
        when(comboService.collectThreadDumpsAndCpuUsage(eq(PID), anyInt(), anyLong())).thenReturn(comboResult);
        when(cpuUsageService.collectFromPid(PID)).thenReturn(successfulCpuUsageResult);
    }

    @Test
    public void doesThreadDumpHandleFailedOutcome() throws Exception {
        final String topFailureReason = "If you run in circles.";
        final String mbeanFailureReason = "Will you ever get anywhere?";
        when(threadDumpService.collectFromPid(PID)).thenReturn(new ThreadDumpResult(asList(topFailureReason, mbeanFailureReason), "", ""));

        mvc.perform(get(COLLECT_TDUMP_PATH + PID)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(""))
                .andExpect(jsonPath("$.failureReasons[0]").value(topFailureReason))
                .andExpect(jsonPath("$.failureReasons[1]").value(mbeanFailureReason));
    }

    @Test
    public void doesThreadDumpWithCollectorWork() throws Exception {
        mvc.perform(get(COLLECT_TDUMP_PATH + PID)
                .param("collector", COLLECTOR_NAME))
                .andExpect(status().isOk());

        verify(threadDumpService).collectFromPid(PID, COLLECTOR_NAME);
    }

    @Test
    public void doesThreadDumpInJsonWork() throws Exception {
        assertJsonWorks(COLLECT_TDUMP_PATH + PID, jsonPath("$.result").value(successfulThreadDumpResult.getResult()));
    }

    @Test
    public void doesThreadDumpInTextWork() throws Exception {
        assertTextWorks(COLLECT_TDUMP_PATH + PID, content().string(THREAD_DUMP_RESULT_STRING));
    }

    @Test
    public void doesThreadDumpTxtFileWork() throws Exception {
        assertTxtFileWorks(COLLECT_TDUMP_PATH + PID);
    }

    @Test
    public void doesDiskSpeedHandleIOException() throws Exception {
        when(diskSpeedCollector.collectDiskSpeed(eq(DIR), anyInt())).thenThrow(new IOException("DAMN SON YA DUN GOOFED"));

        assertOnDiskSpeed(APPLICATION_JSON, status().isInternalServerError(), jsonPath("$.error").value(format("Unable to collect disk speed for %s.", DIR)));
    }

    @Test
    public void doesDiskSpeedHandleNotFoundException() throws Exception {
        final String exceptionError = "Can you see anything here what is happening why is it so dark";
        when(diskSpeedCollector.collectDiskSpeed(eq(DIR), anyInt())).thenThrow(new NotFoundException(exceptionError));

        assertOnDiskSpeed(APPLICATION_JSON, status().isNotFound(), jsonPath("$.error").value(exceptionError));
    }

    @Test
    public void doesDiskSpeedinJsonWork() throws Exception {
        assertOnDiskSpeed(APPLICATION_JSON, status().isOk(), content().json(convertObjectToJson(diskSpeedResult)));
    }

    @Test
    public void doesDiskSpeedInTextWork() throws Exception {
        mvc.perform(get(COLLECT_DISKSPEED_PATH)
                .param("dir", DIR)
                .accept(TEXT_PLAIN))
                .andExpect(status().isOk());
    }

    @Test
    public void doesDiskSpeedTxtFileWork() throws Exception {
        mvc.perform(get(COLLECT_DISKSPEED_PATH)
                .param("dir", DIR)
                .accept(APPLICATION_OCTET_STREAM))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_OCTET_STREAM))
                .andExpect(header().string("Content-Disposition", containsString("attachment; filename=results")));
    }

    @Test
    public void doesDiskSpeedWorkWhenSpecifyingRuns() throws Exception {
        mvc.perform(get(COLLECT_DISKSPEED_PATH)
                .param("runs", "18")
                .param("dir", DIR)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(diskSpeedCollector).collectDiskSpeed(DIR, 18);
    }

    @Test
    public void doExceptionsInTextWork() throws Exception {
        final String message = "The spines. I can't reticulate them!! MY CPU IS NOT WORTHY";
        when(diskSpeedCollector.collectDiskSpeed(eq(DIR), anyInt())).thenThrow(new IOException(message));

        final String result = mvc.perform(get(COLLECT_DISKSPEED_PATH)
                .param("dir", DIR)
                .accept(TEXT_PLAIN))
                .andExpect(status().isInternalServerError())
                .andReturn().getResponse().getContentAsString();

        assertThat(result, containsString(message));
    }

    @Test
    public void doComboResultWorkWithDefaults() throws Exception {
        assertJsonWorks(COLLECT_COMBO_PATH + PID, content().json(convertObjectToJson(comboResult)));

        verify(comboService).collectThreadDumpsAndCpuUsage(PID, 6, 10000);
    }

    @Test
    public void doComboResultWorkWithDefaultsInText() throws Exception {
        assertTextWorks(COLLECT_COMBO_PATH + PID, content().string(containsString(comboResult.toString())));
    }

    @Test
    public void doComboResultWorkWithTxtFile() throws Exception {
        assertTxtFileWorks(COLLECT_COMBO_PATH + PID);
    }

    @Test
    public void doComboResultWorkWithTotal15Wait5() throws Exception {
        mvc.perform(get(COLLECT_COMBO_PATH + PID)
                .param("totalTimes", "15")
                .param("waitTime", "5")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(convertObjectToJson(comboResult)));

        verify(comboService).collectThreadDumpsAndCpuUsage(PID, 15, 5);
    }

    @Test
    public void doesCpuUsageHandleFailedOutcome() throws Exception {
        final String nixFailureReason = "But with one step backward taken \\ I saved myself from going.";
        final String mbeanFailureReason = "A world torn loose went by me.";
        when(cpuUsageService.collectFromPid(PID)).thenReturn(new CpuUsageResult(asList(nixFailureReason, mbeanFailureReason), "", ""));

        mvc.perform(get(COLLECT_CPU_PATH + PID)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(""))
                .andExpect(jsonPath("$.failureReasons[0]").value(nixFailureReason))
                .andExpect(jsonPath("$.failureReasons[1]").value(mbeanFailureReason));
    }

    @Test
    public void doesCpuUsageHandleNotFoundException() throws Exception {
        final String exceptionError = "So where the bloody hell are you?";
        when(cpuUsageService.collectFromPid(eq(PID), eq("smee"))).thenThrow(new NotFoundException(exceptionError));

        mvc.perform(get(COLLECT_CPU_PATH + PID)
                .param("collector", "smee")
                .accept(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error").value(exceptionError));
    }

    @Test
    public void doesCpuUsageWithCollectorWork() throws Exception {
        mvc.perform(get(COLLECT_CPU_PATH + PID)
                .param("collector", COLLECTOR_NAME))
                .andExpect(status().isOk());

        verify(cpuUsageService).collectFromPid(PID, COLLECTOR_NAME);
    }

    @Test
    public void doesCpuUsageInJsonWork() throws Exception {
        assertJsonWorks(COLLECT_CPU_PATH + PID, jsonPath("$.result").value(successfulCpuUsageResult.getResult()));
    }

    @Test
    public void doesCpuUsageInTextWork() throws Exception {
        assertTextWorks(COLLECT_CPU_PATH + PID, content().string(CPU_RESULT_STRING));
    }

    @Test
    public void doesCpuUsageInTxtFileWork() throws Exception {
        assertTxtFileWorks(COLLECT_CPU_PATH + PID);
    }

    private void assertTextWorks(String url, ResultMatcher resultMatcher) throws Exception {
        mvc.perform(get(url)
                .accept(TEXT_PLAIN))
                .andExpect(status().isOk())
                .andExpect(resultMatcher);
    }

    private void assertOnDiskSpeed(MediaType mediaType, ResultMatcher statusMatcher, ResultMatcher jsonMatcher) throws Exception {
        mvc.perform(get(COLLECT_DISKSPEED_PATH)
                .param("dir", DIR)
                .accept(mediaType))
                .andExpect(statusMatcher)
                .andExpect(jsonMatcher);
    }

    private void assertJsonWorks(String url, ResultMatcher resultMatcher) throws Exception {
        mvc.perform(get(url)
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(resultMatcher);
    }

    private void assertTxtFileWorks(String url) throws Exception {
        mvc.perform(get(url)
                .accept(APPLICATION_OCTET_STREAM))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_OCTET_STREAM))
                .andExpect(header().string("Content-Disposition", containsString("attachment; filename=results")));
    }
}