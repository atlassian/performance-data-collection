package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.collectors.results.CumulativeResult;
import com.atlassianlabs.perftools.collectors.threaddump.JCmdThreadDump;
import com.atlassianlabs.perftools.collectors.threaddump.JStackThreadDump;
import com.atlassianlabs.perftools.collectors.threaddump.MBeanThreadDump;
import com.atlassianlabs.perftools.collectors.threaddump.ThreadDumpCollector;
import com.atlassianlabs.perftools.rest.exceptions.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.Map;

import static com.atlassianlabs.perftools.helper.MapHelper.asMap;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ThreadDumpServiceTest {
    private static final long PID = 4242L;
    private static final String DUMP = "meowmeowmeowwoof";
    private static final String JSTACK_FAILURE_REASON = "wind!";
    private static final String MBEAN_FAILURE_REASON = "water!";
    private static final String JCMD_FAILURE_REASON = "earth!";

    @Mock
    private AnalyticsService analyticsService;
    @Mock
    private ApplicationContext context;
    @Mock
    private JStackThreadDump jStackThreadDump;
    @Mock
    private MBeanThreadDump mBeanThreadDump;
    @Mock
    private JCmdThreadDump jCmdThreadDump;

    @InjectMocks
    private ThreadDumpService service;

    @Before
    public void setUp() throws Exception {
        when(context.getBeansOfType(ThreadDumpCollector.class)).thenReturn(asMap(
                "JCmdThreadDump", jCmdThreadDump,
                "MBeanThreadDump", mBeanThreadDump,
                "JStackThreadDump", jStackThreadDump
        ));

        // Use the actual priority here rather than mocking it, cause we want to test the real thing.
        when(jCmdThreadDump.getPriority()).thenCallRealMethod();
        when(jCmdThreadDump.getName()).thenReturn("JCmdThreadDump");
        when(jCmdThreadDump.collectFromProcess(PID)).thenReturn(DUMP);
        when(mBeanThreadDump.getPriority()).thenCallRealMethod();
        when(mBeanThreadDump.getName()).thenReturn("MBeanThreadDump");
        when(jStackThreadDump.getPriority()).thenCallRealMethod();
        when(jStackThreadDump.getName()).thenReturn("JStackThreadDump");
    }

    @Test
    public void doesGetThreadDumpGeneratorsUsePriority() {
        final Map<String, ThreadDumpCollector> collectors = service.getCollectors();

        assertThat(asList(collectors.values().toArray()), is(asList(jCmdThreadDump, jStackThreadDump, mBeanThreadDump)));
    }

    @Test
    public void ifSecondGeneratorSucceedsDoWeStop() throws Exception {
        when(jStackThreadDump.collectFromProcess(PID)).thenReturn(DUMP);
        when(jCmdThreadDump.collectFromProcess(PID)).thenThrow(new IOException(JCMD_FAILURE_REASON));

        final CumulativeResult result = service.collectFromPid(PID);

        assertThat(result.getResult(), is(DUMP));
        verify(jCmdThreadDump).collectFromProcess(PID);
        verify(jStackThreadDump).collectFromProcess(PID);
        verify(mBeanThreadDump, times(0)).collectFromProcess(PID);
    }

    @Test
    public void ifAllGeneratorsFailDoWeSeeFailureReasonsAndNoResults() throws Exception {
        when(jCmdThreadDump.collectFromProcess(PID)).thenThrow(new IOException(JCMD_FAILURE_REASON));
        when(jStackThreadDump.collectFromProcess(PID)).thenThrow(new IOException(JSTACK_FAILURE_REASON));
        when(mBeanThreadDump.collectFromProcess(PID)).thenThrow(new IOException(MBEAN_FAILURE_REASON));

        final CumulativeResult result = service.collectFromPid(PID);

        assertThat(result.getResult(), isEmptyOrNullString());
        assertThat(result.getFailureReasons(), hasSize(3));
        assertThat(result.getFailureReasons().get(0), containsString(JCMD_FAILURE_REASON));
        assertThat(result.getFailureReasons().get(1), containsString(JSTACK_FAILURE_REASON));
        assertThat(result.getFailureReasons().get(2), containsString(MBEAN_FAILURE_REASON));
    }

    @Test(expected = NotFoundException.class)
    public void ifGeneratorIdIsOutOfBoundsDoWeRethrowException() throws Exception {
        service.collectFromPid(PID, "smoochadoopotatoroo");
    }

    @Test
    public void ifWeSpecifyGeneratorIdDoesItOnlyUseThatOne() throws Exception {
        service.collectFromPid(PID, "JStackThreadDump");

        verify(jCmdThreadDump, times(0)).collectFromProcess(PID);
        verify(jStackThreadDump).collectFromProcess(PID);
        verify(mBeanThreadDump, times(0)).collectFromProcess(PID);
    }

    @Test
    public void doesGeneratedByGetSet() {
        final CumulativeResult result = service.collectFromPid(PID);

        assertThat(result.getGeneratedBy(), is(jCmdThreadDump.getName()));
    }
}
