package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.JsonConverter;
import com.atlassianlabs.perftools.analytics.Event;
import com.atlassianlabs.perftools.analytics.EventFactory;
import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.http.HttpClientFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import static java.util.Collections.emptyMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AnalyticsServiceTest {
    private static final String USER_ID = "abc1234ree";

    @Mock
    private HttpClientFactory httpClientFactory;
    @Mock
    private CloseableHttpClient httpClient;
    @Mock
    private TimeLimitedExecutor timeLimitedExecutor;
    @Mock
    private EventFactory eventFactory;

    private AnalyticsService service;

    @Before
    public void setUp() {
        when(httpClientFactory.newHttpClient(anyInt())).thenReturn(httpClient);

        service = new AnalyticsService(httpClientFactory, timeLimitedExecutor, eventFactory);

        when(timeLimitedExecutor.runAsync(any(Callable.class))).thenAnswer(invocation -> {
            final Object[] args = invocation.getArguments();
            final Callable callable = (Callable) args[0];
            return callable.call();
        });
    }

    @Test
    public void itShouldSendEventAsExpected() throws IOException {
        final Event event = new Event(USER_ID, "test.json.event.type", emptyMap(), emptyMap(), 1234L);

        service.sendAsyncEvent(event);

        final ArgumentCaptor<HttpPost> captor = ArgumentCaptor.forClass(HttpPost.class);
        verify(httpClient).execute(captor.capture());
        final HttpPost httpPost = captor.getValue();
        final List<NameValuePair> params = URLEncodedUtils.parse(httpPost.getEntity());

        assertThat(httpPost.getURI().toString(), equalTo("https://api.amplitude.com/httpapi"));
        assertThat(params.get(0).getName(), equalTo("api_key"));
        assertThat(params.get(0).getValue(), equalTo("d7f700f458452bc002fa7041a613ed63"));
        assertThat(params.get(1).getName(), equalTo("event"));
        assertThat(params.get(1).getValue(), equalTo(JsonConverter.convertObjectToJson(event)));
    }

    @Test
    public void itShouldNotThrowExceptionIfHttpClientFails() throws Exception {
        when(httpClient.execute(any())).thenThrow(IOException.class);

        service.sendAsyncEvent(new Event(USER_ID, "ree", emptyMap(), emptyMap(), 1234L));
    }
}
