package com.atlassianlabs.perftools.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileSystemServiceTest {
    private static final String JIRA_LINUX_HOME = "/var/atlassian/application-data/jira";
    private static final String JIRA_WINDOWS_HOME = "C:\\Program Files\\Atlassian\\Application Data\\JIRA7.2.1";

    private static final String CONF_LINUX_HOME = "/var/atlassian/application-data/confluence";
    private static final String CONF_WINDOWS_HOME = "C:\\Program Files\\Atlassian\\Application Data\\Confluence";

    private FileSystemService fileSystemService;

    private File testFile;
    private Properties properties;

    @Before
    public void setUp() throws Exception {
        testFile = null;
        properties = null;

        fileSystemService = new FileSystemService();
    }

    @Test
    public void parseProperties_jira_linux() throws Exception {
        testFile = new File("src/test/resources/home_files/linux/jira-application.properties");

        properties = fileSystemService.parsePropertiesFromFile(testFile);

        assertThat("jira.home", properties.getProperty("jira.home"), is(equalTo(JIRA_LINUX_HOME)));
    }

    @Test
    public void parseProperties_jira_windows() throws Exception {
        testFile = new File("src/test/resources/home_files/windows/jira-application.properties");

        properties = fileSystemService.parsePropertiesFromFile(testFile);
        assertThat("jira.home", properties.getProperty("jira.home"), is(equalTo(JIRA_WINDOWS_HOME)));
    }

    @Test
    public void parseProperties_confluence_linux() throws Exception {
        testFile = new File("src/test/resources/home_files/linux/confluence-init.properties");

        properties = fileSystemService.parsePropertiesFromFile(testFile);

        assertThat("confluence.home", properties.getProperty("confluence.home"), is(equalTo(CONF_LINUX_HOME)));
    }

    @Test
    public void parseProperties_confluence_windows() throws Exception {
        testFile = new File("src/test/resources/home_files/windows/confluence-init.properties");

        properties = fileSystemService.parsePropertiesFromFile(testFile);

        assertThat("confluence.home", properties.getProperty("confluence.home"), is(equalTo(CONF_WINDOWS_HOME)));
    }

    @Test
    public void isValidFile_dir_and_file_exists() throws Exception {
        testFile = mock(File.class);

        when(testFile.exists()).thenReturn(true);
        when(testFile.isDirectory()).thenReturn(true);

        assertThat(fileSystemService.isValidFile(testFile), is(equalTo(false)));
    }

    @Test
    public void isValidFile_not_exists() throws Exception {
        testFile = mock(File.class);

        when(testFile.exists()).thenReturn(false);

        assertThat(fileSystemService.isValidFile(testFile), is(equalTo(false)));
    }

    @Test
    public void isValidFile_exists_and_not_dir() throws Exception {
        testFile = mock(File.class);

        when(testFile.exists()).thenReturn(true);
        when(testFile.isDirectory()).thenReturn(false);

        assertThat(fileSystemService.isValidFile(testFile), is(equalTo(true)));
    }
}