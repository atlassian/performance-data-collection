package com.atlassianlabs.perftools;

import org.gridkit.lab.jvm.attach.JavaProcessId;

public class PublicJavaProcessId extends JavaProcessId {

    // So we have a public constructor for testing.
    public PublicJavaProcessId(long pid, String description) {
        super(pid, description);
    }
}
