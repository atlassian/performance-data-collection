@echo off
setlocal
setlocal enabledelayedexpansion

set _PRG_DIR=%~dp0
cd %_PRG_DIR%
rem %SERVICE_EXE% will end up something like "C:\Program Files\Atlassian\data-collector\windows-service\service.exe" (with the actual quotes)
set SERVICE_EXE="%_PRG_DIR%\windows-service\service.exe"

rem Check for sudo access, otherwise we cannot install:
net session >nul 2>&1
if errorlevel 1 goto :notAdmin

rem Check in path for Javac (if that exists, so will java):
javac -version >nul 2>&1
if not errorlevel 1 goto :foundJava

:noJava
echo Unable to locate Java JDK in the Windows PATH, which is required to run this tool. Add the JDK bin directory to the PATH to fix this.
goto :end

:foundJava
echo Java JDK located. Checking if data-collector service exists.

:checkService
for /f %%i in ('%SERVICE_EXE% status') do set serviceStatus=%%i
if %serviceStatus% == NonExistent goto :installService

:serviceExists
echo The data-collector service already exists and will not be installed.
goto :end

:notAdmin
echo Unable to install service, as not logged in as an administrator. Right click ^> Run as Administrator to fix this.
goto :end

:installService
echo Attempting to install data-collector service.
rem Because this is put in quotes above, this will be "C:\Program Files\Atlassian\data-collector\windows-service\service.exe" install
%SERVICE_EXE% install
if errorlevel 1 goto :end

:startService
echo Attempting to start data-collector service.
%SERVICE_EXE% start
if not errorlevel 1 goto :checkRunning

:checkRunning
echo Waiting for the service to start...
timeout /t 3 > NUL
for /f %%i in ('%SERVICE_EXE% status') do set serviceStatus=%%i
if %serviceStatus% == Started goto :end
goto :didNotStart

:didNotStart
echo The service did not appear to be able to start successfully. Please check the logs directory for any errors.
goto :end

:end
pause
