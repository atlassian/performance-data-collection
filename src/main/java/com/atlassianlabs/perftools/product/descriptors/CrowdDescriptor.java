package com.atlassianlabs.perftools.product.descriptors;

import org.springframework.stereotype.Component;

@Component
public class CrowdDescriptor implements ProductDescriptor {

    @Override
    public String getDisplayName() {
        return "org.apache.catalina.startup.Bootstrap start";
    }

    @Override
    public String getInstProp() {
        return "catalina.base";
    }

    @Override
    public String getHomeProp() {
        return "crowd.home";
    }

    @Override
    public String getHomeFile() {
        // catalina.base for Crowd has apache-tomcat in it, e.g.: /Users/valhallabeastmode/Downloads/atlassian-crowd-2.10.1/apache-tomcat
        // this is why there is a .. before this.
        return "../crowd-webapp/WEB-INF/classes/crowd-init.properties";
    }

    @Override
    public String getLogPath() {
        return "logs";
    }

    @Override
    public String getName() {
        return "Crowd";
    }
}
