package com.atlassianlabs.perftools.product.descriptors;

/**
 * In order to identify products, we need to to have attributes that describe them.
 */
public interface ProductDescriptor {

    /**
     * @return The partial display name of the Java process that is used to describe the Java process.
     */
    String getDisplayName();

    /**
     * @return The Java property name that defines the installation directory, e.g.: catalina.home.
     */
    String getInstProp();

    /**
     * @return The Java property name that defines the home directory, e.g.: jira.home.
     */
    String getHomeProp();

    /**
     * This is used to append to the installation directory for the location of the home file, e.g.: confluence-init.properties.
     * If this is null we won't try to parse the file, as for certain products it doesn't exist.
     *
     * @return The string of the path to the file where the home property is stored.
     */
    default String getHomeFile() {
        return null;
    };

    /**
     * This is appended to the home location, where log file information is stored. This was previously used to output
     * files so the Support Tools Plugin would pick them up, however STP needs to be updated to do this. Leaving here for
     * posterity.
     *
     * @return The location of the log path.
     */
    String getLogPath();

    /**
     * @return The name of the product.
     */
    String getName();
}
