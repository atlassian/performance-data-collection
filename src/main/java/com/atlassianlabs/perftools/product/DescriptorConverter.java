package com.atlassianlabs.perftools.product;

import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.product.descriptors.ProductDescriptor;
import com.atlassianlabs.perftools.services.FileSystemService;
import org.gridkit.lab.jvm.attach.JavaProcessDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * Products are fairly difficult to construct as we need to:
 * <p>
 * 1) Check the process has a valid Java display name
 * 2) See if we can find the install dir from the system properties
 * 3) Check if the home dir is set as a system property
 * 4) If not, check if the home properties file exists (e.g.: jira-application.properties)
 * 5) If it does, parse it to find the actual home path
 * 6) Then find the log directory (so we can optionally output data files to it)
 * 7) Return the constructed Product, wrapped in an Optional
 * </p>
 * This class is responsible for checking all of this against a {@link ProductDescriptor} object, one of which should
 * be implemented for each available product.
 */
public class DescriptorConverter implements Function<ProductDescriptor, Optional<Product>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DescriptorConverter.class);

    private final JavaProcessDetails processDetails;
    private final FileSystemService fileSystemService;
    private final TimeLimitedExecutor timeLimitedExecutor;


    public DescriptorConverter(@NotNull FileSystemService fileSystemService,
                               @NotNull TimeLimitedExecutor timeLimitedExecutor,
                               @NotNull JavaProcessDetails processDetails) {
        this.fileSystemService = requireNonNull(fileSystemService);
        this.timeLimitedExecutor = requireNonNull(timeLimitedExecutor);
        this.processDetails = requireNonNull(processDetails);
    }

    /**
     * Checks if the provided processDetails matches the provided {@link ProductDescriptor}.
     *
     * @param descriptor The product descriptor to compare against the process details.
     * @return An optional of the Product - if it has matched the descriptor it will be the fully constructed product.
     */
    @Override
    public Optional<Product> apply(ProductDescriptor descriptor) {
        try {
            return timeLimitedExecutor.runSync(() -> {
                LOGGER.debug("Checking Java process '{}' against descriptor '{}'.", processDetails.getDescription(),
                        descriptor.getName());

                // We can easily exclude certain processes by looking at the display name and checking if it matches:
                if (fileSystemService.isUnix()) {
                    final String displayName = descriptor.getDisplayName();
                    if (!processDetails.getDescription().contains(displayName)) {
                        LOGGER.debug("Java process '{}' does not match descriptor display name '{}'", processDetails.getDescription(), displayName);
                        return productNotIdentified();
                    }
                }

                // The install dir is needed to get the home dir properties file.
                final Properties properties = processDetails.getSystemProperties();
                final String installDir = properties.getProperty(descriptor.getInstProp());
                LOGGER.debug("Install dir: '{}'.", installDir);

                if (installDir == null) {
                    return productNotIdentified();
                }

                // Try to get the home property from the system properties,
                String homeDir = properties.getProperty(descriptor.getHomeProp());
                LOGGER.debug("Home dir: '{}'.", homeDir);

                // Try to populate the homeDir by parsing the file if it's not in the system properties, if it has a home
                // file. Some products don't have home files, e.g.: FeCru / Bitbucket Server
                if (homeDir == null && descriptor.getHomeFile() != null) {
                    final File homeDirFile = new File(installDir, descriptor.getHomeFile());
                    LOGGER.debug("Home dir file to see if valid: '{}'.", homeDirFile);

                    if (fileSystemService.isValidFile(homeDirFile)) {
                        try {
                            homeDir = fileSystemService.parsePropertiesFromFile(homeDirFile).getProperty(descriptor.getHomeProp());
                        } catch (IOException e) {
                            LOGGER.info("Unable to parse the file '{}' due to {}", homeDirFile, e);
                            return Optional.empty();
                        }
                        LOGGER.debug("Home dir file is valid, parsed directory: '{}'.", homeDir);
                    }
                }
                // Might not exist, if so this isn't the product:
                if (homeDir == null) {
                    return productNotIdentified();
                } else {
                    final String logFileDir = homeDir + File.separator + descriptor.getLogPath();
                    LOGGER.debug("Log file dir: '{}'", logFileDir);

                    Product product = new Product(processDetails.getPid(), descriptor.getName(), homeDir, installDir, logFileDir);
                    LOGGER.debug("Identified product: '{}'.", product);

                    return Optional.of(product);
                }
            });
        } catch (Exception e) {
            LOGGER.info("Unable to lookup product for Java process '{}' as execution failed", processDetails.getDescription(), e);
            return Optional.empty();
        }
    }

    private Optional<Product> productNotIdentified() {
        LOGGER.debug("No product identified for Java process '{}'", processDetails.getDescription());
        return Optional.empty();
    }
}
