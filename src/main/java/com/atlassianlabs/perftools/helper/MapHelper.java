package com.atlassianlabs.perftools.helper;

import com.google.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.collect.ImmutableMap.copyOf;

/**
 * This class provides additional functionality that doesn't currently exist within {@link java.util.Map}.
 */
public class MapHelper {
    public MapHelper() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Creates a {@link HashMap} of the provided key and values. This method provides a bridge
     * similar to {@link java.util.Arrays#asList(Object[])}, except for Maps.
     *
     * @param key        first key of the map
     * @param value      first value of the map
     * @param keysValues pairs of key value
     *                   Example: asMap(1, 2, 2, 4) will create {@literal {1 -> 2, 2 -> 4}}
     * @return new {@link ImmutableMap} of the provided key {@literal ->} value pairs
     */
    @SuppressWarnings("unchecked")
    public static <K, V> Map<K, V> asMap(K key, V value, Object... keysValues) {
        final Map<K, V> res = new HashMap<>();
        res.put(key, value);
        if (keysValues.length > 0) {
            if (keysValues.length % 2 > 0) {
                throw new IllegalArgumentException("Arguments count must be even!");
            }
            for (int i = 0; i < keysValues.length; i += 2) {
                K k = (K) keysValues[i];
                V v = (V) keysValues[i + 1];
                res.put(k, v);
            }
        }
        return copyOf(res);
    }
}
