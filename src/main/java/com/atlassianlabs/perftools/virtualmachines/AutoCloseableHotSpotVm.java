package com.atlassianlabs.perftools.virtualmachines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Provides an {@link AutoCloseable} wrapper for a sun.tools.attach.HotSpotVirtualMachine, and also has some delegating methods
 * to the underlying VM. It holds an object reference and uses reflection to get access to the VM as otherwise
 * we need to bundle sun.tools in the uber-jar, and that is real bad for diff version compatibility in Java.
 */
public class AutoCloseableHotSpotVm implements AutoCloseable {
    private static final Logger LOGGER = LoggerFactory.getLogger(AutoCloseableHotSpotVm.class);
    private final Object hotSpotVirtualMachineObject;
    private Class<?> hotSpotVirtualMachineClass;

    AutoCloseableHotSpotVm(final Object hotSpotVirtualMachineObject) throws ClassNotFoundException {
        this.hotSpotVirtualMachineObject = hotSpotVirtualMachineObject;
        this.hotSpotVirtualMachineClass = Class.forName("sun.tools.attach.HotSpotVirtualMachine");
    }

    /**
     * Delegates to HotSpotVirtualMachine#executeJCmd(java.lang.String).
     *
     * @param cmd Command to execute (e.g.: Thread.print)
     * @return Results of the executed JCmd
     * @throws IOException               if there is a problem getting the InputStream from HotSpotVirtualMachine#executeJCmd(java.lang.String)
     * @throws NoSuchMethodException     if HotSpotVirtualMachine#executeJCmd(java.lang.String) cannot be located
     * @throws InvocationTargetException if an exception is thrown invoking HotSpotVirtualMachine#executeJCmd(java.lang.String)
     * @throws IllegalAccessException    if HotSpotVirtualMachine#executeJCmd(java.lang.String) cannot be accessed
     */
    public InputStream executeJCmd(String cmd) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        LOGGER.debug("Executing Jcmd {}", cmd);
        final Method method = hotSpotVirtualMachineClass.getDeclaredMethod("executeJCmd", String.class);
        return (InputStream) method.invoke(hotSpotVirtualMachineObject, cmd);
    }

    /**
     * Delegates to HotSpotVirtualMachine#remoteDataDump(java.lang.Object...).
     *
     * @param args Args to send, such as force or lock data.
     * @return Results of the remote data dump
     * @throws IOException               if there is a problem getting the InputStream from HotSpotVirtualMachine#remoteDataDump(java.lang.Object...)
     * @throws NoSuchMethodException     if HotSpotVirtualMachine#remoteDataDump(java.lang.Object...) cannot be located
     * @throws InvocationTargetException if an exception is thrown invoking HotSpotVirtualMachine#remoteDataDump(java.lang.Object...)
     * @throws IllegalAccessException    if HotSpotVirtualMachine#remoteDataDump(java.lang.Object...) cannot be accessed
     */
    public InputStream remoteDataDump(Object... args) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        LOGGER.debug("Remote data dumping with args '{}'", args);
        final Method method = hotSpotVirtualMachineClass.getDeclaredMethod("remoteDataDump", Object[].class);
        // We need to put the args inside a new Object[] as otherwise varargs handling gon goof if it's an empty argument.
        return (InputStream) method.invoke(hotSpotVirtualMachineObject, new Object[]{args});
    }

    public Object getHotSpotVirtualMachineObject() {
        return hotSpotVirtualMachineObject;
    }

    @Override
    public void close() throws Exception {
        final Method method = hotSpotVirtualMachineClass.getMethod("detach");
        method.invoke(hotSpotVirtualMachineObject);
    }
}
