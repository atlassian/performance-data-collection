package com.atlassianlabs.perftools.virtualmachines;

import com.atlassianlabs.perftools.services.VirtualMachineService;
import org.gridkit.jvmtool.MBeanCpuUsageReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ConnectException;

import static java.util.Objects.requireNonNull;

/**
 * This factory exists to construct virtual machine objects, such as the HotSpotVirtualMachine and the cpu usage reporter.
 */
@Component
public class VirtualMachineFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(VirtualMachineFactory.class);
    private final VirtualMachineService virtualmachineservice;

    @Autowired
    public VirtualMachineFactory(@NotNull final VirtualMachineService virtualmachineservice) {
        this.virtualmachineservice = requireNonNull(virtualmachineservice);
    }

    /**
     * Gets a MBeanServerConnection of the provided pid then uses it to construct and return a {@link MBeanCpuUsageReporter}.
     *
     * @param pid {@link String} of the process id.
     * @return A {@link MBeanCpuUsageReporter} that can be used for reporting on CPU usage.
     * @throws ConnectException  if the Java process cannot be connected to with the mbean server.
     */
    public MBeanCpuUsageReporter getMBeanCpuUsageReporter(long pid) throws ConnectException {
        LOGGER.debug("Attempting to collect cpu usage for pid {}", pid);
        return new MBeanCpuUsageReporter(virtualmachineservice.getMBeanServerConnection(pid));
    }

    /**
     * Attempts to bind to the provided process id using the sun.tools attach library. Reflection is used to get access
     * to these APIs as otherwise we need to bundle sun.tools and that gets even messier than using reflection.
     *
     * @param pid The process ID to attach to.
     * @return A {@link AutoCloseableHotSpotVm}, which is an autocloseable HotSpotVirtualMachine.
     * @throws ClassNotFoundException    if com.sun.tools.attach.VirtualMachine is not found
     * @throws NoSuchMethodException     if VirtualMachine#attach(java.lang.String) is not found
     * @throws InvocationTargetException if VirtualMachine#attach(java.lang.String) cannot be invoked
     * @throws IllegalAccessException    if any methods that are called are not accessible
     */
    public AutoCloseableHotSpotVm getHotSpotVirtualMachine(long pid) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        LOGGER.debug("Attempting to get hotspot vm for pid {}", pid);
        final Class<?> virtualMachine = Class.forName("com.sun.tools.attach.VirtualMachine");
        final Method method = virtualMachine.getMethod("attach", String.class);
        // Use null because static:
        return new AutoCloseableHotSpotVm(method.invoke(null, String.valueOf(pid)));
    }
}
