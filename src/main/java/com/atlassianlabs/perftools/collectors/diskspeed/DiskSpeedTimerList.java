package com.atlassianlabs.perftools.collectors.diskspeed;

import com.google.common.annotations.VisibleForTesting;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

import static com.atlassianlabs.perftools.collectors.diskspeed.Util.format;

/**
 * This provides a list of timers that can be used to record different operations. It also provides the results of them.
 * It is expected to only be used by {@link DiskSpeedOperation}.
 */
public class DiskSpeedTimerList {
    private final List<DiskSpeedTimer> timers;

    DiskSpeedTimerList() {
        this.timers = new ArrayList<>();
    }

    /**
     * Construct a new timer, add it to the internal list of timers, start it and return it.
     *
     * @return A new, started timer.
     */
    DiskSpeedTimer newTimer() {
        final DiskSpeedTimer timer = new DiskSpeedTimer();
        timers.add(timer);
        return timer.start();
    }

    @VisibleForTesting
    void addTimer(DiskSpeedTimer timer) {
        timers.add(timer);
    }

    @ApiModelProperty(value = "The average speed in nanoseconds", example = "55,645", required = true)
    public String getAverage() {
        long count = 0;
        for (DiskSpeedTimer timer : timers) {
            count = count + timer.getTotal();
        }
        return format(count / timers.size());
    }

    @ApiModelProperty(value = "The median speed in nanoseconds", example = "47,527", required = true)
    public String getMedian() {
        return format(timers.stream().sorted().skip(timers.size() / 2).findFirst().get().getTotal());
    }

    @ApiModelProperty(value = "The maximum speed in nanoseconds", example = "902,148", required = true)
    public String getMax() {
        long max = 0;
        for (DiskSpeedTimer timer : timers) {
            max = Math.max(max, timer.getTotal());
        }
        return format(max);
    }

    @ApiModelProperty(value = "The minimum speed in nanoseconds", example = "30,570", required = true)
    public String getMin() {
        long min = Long.MAX_VALUE;
        for (DiskSpeedTimer timer : timers) {
            min = Math.min(min, timer.getTotal());
        }
        return format(min);
    }
}