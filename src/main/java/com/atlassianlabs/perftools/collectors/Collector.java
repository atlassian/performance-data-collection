package com.atlassianlabs.perftools.collectors;

public interface Collector {
    /**
     * Collects data using the provided process id. Collection approach is up to the implementation.
     *
     * @param pid The process id to collect data from
     * @return The data collected in String form
     * @throws Exception if something goes wrong
     */
    String collectFromProcess(long pid) throws Exception;

    /**
     * Used to determine which implementation should be invoked first. Lower indicates higher priority.
     *
     * @return The priority of the implementation, lower comes first.
     */
    int getPriority();

    /**
     * @return The name of the collector.
     */
    String getName();
}
