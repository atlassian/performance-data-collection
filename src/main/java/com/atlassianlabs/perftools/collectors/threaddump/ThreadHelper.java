package com.atlassianlabs.perftools.collectors.threaddump;

import java.lang.management.ThreadInfo;

class ThreadHelper {
    ThreadHelper() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * As it is displayed in 1st line of thread entry in thread dump
     *
     * @param ti thread info
     * @return string of thread state
     */
    static String getThreadState(ThreadInfo ti) {
        final StackTraceElement topOfStackTrace = getTopOfStackTrace(ti);

        switch (ti.getThreadState()) {
            case RUNNABLE:
                return "runnable";

            case BLOCKED:
                return "waiting for monitor entry";

            case WAITING:
                if (isObjectWait(topOfStackTrace)) {
                    return "in Object.wait()";
                } else {
                    return "waiting on condition";
                }

            case TIMED_WAITING:
                if (isThreadSleep(topOfStackTrace)) {
                    return "sleeping";
                } else if (isObjectWait(topOfStackTrace)) {
                    return "in Object.wait()";
                } else {
                    return "waiting on condition";
                }

            default:
                return "thread-state";
        }
    }

    /**
     * As it is displayed in 2nd line of thread entry in thread dump
     *
     * @param ti thread info
     * @return string of thread status name
     */
    static String getThreadStatusName(ThreadInfo ti) {
        final StackTraceElement topOfStackTrace = getTopOfStackTrace(ti);

        switch (ti.getThreadState()) {
            case BLOCKED:
                return "BLOCKED (on object monitor)";

            case WAITING:
                if (isObjectWait(topOfStackTrace)) {
                    return "WAITING (on object monitor)";
                } else {
                    return "WAITING (parking)";
                }

            case TIMED_WAITING:
                if (isThreadSleep(topOfStackTrace)) {
                    return "TIMED_WAITING (sleeping)";
                } else if (isObjectWait(topOfStackTrace)) {
                    return "TIMED_WAITING (on object monitor)";
                } else {
                    return "TIMED_WAITING (parking)";
                }

            default:
                return ti.getThreadState() == null ? "UNKNOWN" : ti.getThreadState().toString().toUpperCase();
        }
    }

    static boolean isObjectWait(StackTraceElement element) {
        return element != null &&
                Object.class.getName().equals(element.getClassName()) && "wait".equals(element.getMethodName());
    }

    private static boolean isThreadSleep(StackTraceElement element) {
        return element != null &&
                Thread.class.getName().equals(element.getClassName()) && "sleep".equals(element.getMethodName());
    }

    private static StackTraceElement getTopOfStackTrace(ThreadInfo ti) {
        final StackTraceElement[] stackTrace = ti.getStackTrace();
        return stackTrace.length > 0 ? stackTrace[0] : null;
    }
}