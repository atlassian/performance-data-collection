package com.atlassianlabs.perftools.collectors.threaddump;

import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.virtualmachines.AutoCloseableHotSpotVm;
import com.atlassianlabs.perftools.virtualmachines.VirtualMachineFactory;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.StringWriter;

import static java.util.Objects.requireNonNull;

/**
 * Uses the sun.tools API to bind to a JVM and produce a thread dump. Involves reflection to avoid trying to bundle
 * the sun.tools with the uberjar, as it's Java-version specific and should be taken from the JDK classpath rather than
 * the JAR that this tool is in.
 */
@Component
public class JStackThreadDump implements ThreadDumpCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(JStackThreadDump.class);
    private static final int PRIORITY = 1;

    private final VirtualMachineFactory virtualMachineFactory;
    private final TimeLimitedExecutor timeLimitedExecutor;

    @Autowired
    public JStackThreadDump(@NotNull VirtualMachineFactory virtualMachineFactory, @NotNull final TimeLimitedExecutor timeLimitedExecutor) {
        this.virtualMachineFactory = requireNonNull(virtualMachineFactory);
        this.timeLimitedExecutor = requireNonNull(timeLimitedExecutor);
    }

    /**
     * Uses the same code jstack uses in the sun.tools api, accessed with reflection to collect the thread dump.
     *
     * @param pid The Java process ID to take a thread dump of.
     * @return The thread dump as a string.
     * @throws IOException if there's any exceptions when attempting to take the dump, such as problems
     *                     binding to the VM, constructing the string of the thread dump or executor service failures
     */
    @Override
    public String collectFromProcess(long pid) throws Exception {
        return timeLimitedExecutor.runSync(() -> {
            final StringWriter out = new StringWriter();

            LOGGER.debug("Attempting to bind to pid " + pid);
            try (final AutoCloseableHotSpotVm vm = virtualMachineFactory.getHotSpotVirtualMachine(pid)) {
                LOGGER.debug("Attemping to execute a remote data dump.");

                IOUtils.copy(vm.remoteDataDump(), out);
                return out.toString();
            }
        });
    }

    @Override
    public int getPriority() {
        return PRIORITY;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }
}
