package com.atlassianlabs.perftools.collectors.threaddump;

import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.services.VirtualMachineService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.lang.management.LockInfo;
import java.lang.management.MonitorInfo;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDateTime.now;
import static java.util.Objects.requireNonNull;
import static org.gridkit.jvmtool.stacktrace.ThreadMXBeanEx.BeanHelper.connectThreadMXBean;

/**
 * This method of collecting a thread dump gets access using the mbean server, then iterates through all
 * the threads using the thread MXBean. It's an alternative to using sun.tools and the results can be parsed with tools such as
 * TDA. It does miss a few things that are included in a standard thread dump, however they're not required to identify problems.
 */
@Component
public class MBeanThreadDump implements ThreadDumpCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(MBeanThreadDump.class);
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final int PRIORITY = 2;
    private static final int HEX_SIZE = 16;

    private final VirtualMachineService virtualMachineService;
    private final TimeLimitedExecutor timeLimitedExecutor;

    @Autowired
    public MBeanThreadDump(@NotNull final VirtualMachineService virtualMachineService, @NotNull final TimeLimitedExecutor timeLimitedExecutor) {
        this.virtualMachineService = requireNonNull(virtualMachineService);
        this.timeLimitedExecutor = requireNonNull(timeLimitedExecutor);
    }

    @Override
    public int getPriority() {
        return PRIORITY;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    /**
     * Binds to the pid using a mbean server connection, then gets access to the thread mx bean to programatically
     * collect a thread dump. This does not use Java sun.tools, rather mx beans and our own custom parsers to build a threadie.
     *
     * @param pid The Java process ID to take a thread dump of.
     * @return The data collected in String form.
     * @throws IOException If there is any problem connecting to the virtual machine, or there is an issue in constructing the
     *                     thread dump string.
     */
    public String collectFromProcess(long pid) throws Exception {
        return timeLimitedExecutor.runSync(() -> {
            final ThreadMXBean threadMXBean = connectThreadMXBean(virtualMachineService.getMBeanServerConnection(pid));

            final StringBuffer writer = new StringBuffer();

            printThreadDumpHeader(writer);

            final ThreadInfo[] threadInfoArray = threadMXBean.dumpAllThreads(threadMXBean.isObjectMonitorUsageSupported(), threadMXBean.isSynchronizerUsageSupported());

            for (ThreadInfo threadInfo : threadInfoArray) {
                printThreadInfo(writer, threadInfo);
                printThreadState(writer, threadInfo);
                printStackTrace(writer, threadInfo);

                writer.append("\n");
                printLockedOwnableSynchronizers(writer, threadInfo);
                writer.append("\n");
            }

            writer.append("\"VM Periodic Task Thread\" prio=10 tid=0x0000000000000000 nid=0 fake entry so TDA can understand where thread dump ends\n");
            writer.append("\n");

            return writer.toString();
        });
    }

    private void printThreadDumpHeader(Appendable appendable) throws IOException {
        appendable
                .append(now().format(DATE_FORMAT))
                .append("\n")
                .append("Full thread dump ")
                .append("\n")
                .append("\n");
    }

    /**
     * Takes the provided threadinfo and formats it into the relevant thread state using the provided appendable. This extracts:
     * Name, a faked priority, the thread ID as a hex string and the thread state.
     *
     * @param appendable Whatever we want to append the thread info to, for example a StringWriter.
     * @param ti         The relevant thread info object to write to the appendable.
     * @throws IOException if there is a problem appending the info.
     */
    private void printThreadInfo(Appendable appendable, ThreadInfo ti) throws IOException {
        appendable
                .append('"').append(ti.getThreadName()).append('"')
                .append(" ")
                .append("prio=").append("9")
                .append(" ")
                .append("tid=").append(toHexString(ti.getThreadId()))
                .append(" ")
                .append("nid=0")
                .append(" ")
                .append(ThreadHelper.getThreadState(ti))
                .append(" ")
        ;
        appendable.append("\n");
    }

    private void printThreadState(Appendable appendable, ThreadInfo ti) throws IOException {
        appendable
                .append("   ")
                .append(ti.getThreadState().getClass().getCanonicalName())
                .append(": ")
                .append(ThreadHelper.getThreadStatusName(ti))
                .append("\n");
    }

    private void printStackTrace(Appendable a, ThreadInfo ti) throws IOException {
        StackTraceElement[] stackTrace = ti.getStackTrace();
        final LockInfo lockInfo = ti.getLockInfo();
        for (int frameCount = 0; frameCount < stackTrace.length; frameCount++) {
            StackTraceElement frame = stackTrace[frameCount];
            a.append("\t").append("at ").append(frame.toString()).append("\n");

            if (frameCount == 0) {
                printThreadLockInfo(a, ti, frame);
            }

            // vframe.cpp
            boolean foundFirstMonitor = false;
            for (MonitorInfo mi : ti.getLockedMonitors()) {
                if (mi.getLockedStackDepth() == frameCount) {
                    if (!foundFirstMonitor && frameCount == 0 && ti.getThreadState() == Thread.State.BLOCKED && lockInfo != null) {
                        a.append("\t- waiting to lock ").append(lockInfoToString(lockInfo)).append("\n");
                    }

                    a.append("\t- locked ").append(lockInfoToString(mi)).append("\n");
                    foundFirstMonitor = true;
                }
            }

        }
    }

    /**
     * Prints lock information for the thread
     * <p>
     * The implementation was based on {@link ThreadInfo#getLockInfo()} javadoc and influenced by TDA's {@code MBeanDumper.printThread()}
     */
    private void printThreadLockInfo(Appendable a, ThreadInfo ti, StackTraceElement frame0) throws IOException {
        final LockInfo lockInfo = ti.getLockInfo();
        if (lockInfo != null) {
            switch (ti.getThreadState()) {
                case BLOCKED:
                    a.append("\t- waiting to lock ").append(lockInfoToString(lockInfo)).append("\n");
                    printThreadLockOwner(a, ti);
                    break;
                case WAITING:
                case TIMED_WAITING:
                    if (ThreadHelper.isObjectWait(frame0)) {
                        a.append("\t- waiting on ").append(lockInfoToString(lockInfo)).append("\n");
                    } else {
                        a.append("\t- parking to wait for ").append(lockInfoToString(lockInfo)).append("\n");
                    }
                    printThreadLockOwner(a, ti);
                    break;
                default:
                    LOGGER.warn("Unrecognized thread {} state {} for which lock info is not empty", toHexString(ti.getThreadId()), ti.getThreadState());
                    break;
            }
        }
    }

    private void printThreadLockOwner(Appendable a, ThreadInfo ti) throws IOException {
        if (ti.getLockOwnerName() != null) {
            a.append("\t owned by ").append(ti.getLockOwnerName())
                    .append(" id=").append(toHexString(ti.getLockOwnerId()))
                    .append("\n");
        }
    }

    private String lockInfoToString(LockInfo li) {
        return String.format("<%s> (a %s)",
                toHexString(li.getIdentityHashCode()),
                li.getClassName());
    }

    private void printLockedOwnableSynchronizers(Appendable a, ThreadInfo ti) throws IOException {
        a.append("   Locked ownable synchronizers:").append("\n");

        final LockInfo lockedSynchronizers[] = ti.getLockedSynchronizers();
        if (lockedSynchronizers != null && lockedSynchronizers.length > 0) {
            for (LockInfo li : lockedSynchronizers) {
                a.append("\t").append("- ").append(li.toString()).append("\n");
            }
        } else {
            a.append("\t").append("- None").append("\n");
        }
    }

    private String toHexString(Integer value) {
        return "0x" + StringUtils.leftPad(Integer.toHexString(value), HEX_SIZE, '0');
    }

    private String toHexString(Long value) {
        return "0x" + StringUtils.leftPad(Long.toHexString(value), HEX_SIZE, '0');
    }
}
