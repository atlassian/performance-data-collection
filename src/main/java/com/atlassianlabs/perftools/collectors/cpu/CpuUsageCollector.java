package com.atlassianlabs.perftools.collectors.cpu;

import com.atlassianlabs.perftools.collectors.Collector;

/**
 * Allows for multiple ways to collect cpu usage.
 */
public interface CpuUsageCollector extends Collector {
    /**
     * Allows for cpu usage collection to take an additional sampleTime argument. This may or may not be used depending
     * upon the implementation.
     *
     * @param pid        The process id to collect data for.
     * @param sampleTime The sampleTime that can optionally be used in the implementation.
     * @return The data collected in String form.
     * @throws Exception If there are any exceptions that occur during collection.
     */
    String collectFromPid(long pid, long sampleTime) throws Exception;
}
