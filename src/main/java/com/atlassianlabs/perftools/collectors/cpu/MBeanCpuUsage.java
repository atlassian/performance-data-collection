package com.atlassianlabs.perftools.collectors.cpu;

import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.services.TimeService;
import com.atlassianlabs.perftools.virtualmachines.VirtualMachineFactory;
import org.gridkit.jvmtool.MBeanCpuUsageReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

@Component
public class MBeanCpuUsage implements CpuUsageCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(MBeanCpuUsage.class);
    private static final long MBEAN_CPU_SAMPLE_SLEEP = Integer.getInteger("mbean.cpu.sample.sleep", (int) TimeUnit.SECONDS.toMillis(1));
    private static final int PRIORITY = 1;

    private final VirtualMachineFactory virtualMachineFactory;
    private final TimeService timeService;
    private final TimeLimitedExecutor timeLimitedExecutor;

    @Autowired
    public MBeanCpuUsage(@NotNull final VirtualMachineFactory virtualMachineFactory,
                         @NotNull final TimeService timeService,
                         @NotNull final TimeLimitedExecutor timeLimitedExecutor) {
        this.virtualMachineFactory = requireNonNull(virtualMachineFactory);
        this.timeService = requireNonNull(timeService);
        this.timeLimitedExecutor = requireNonNull(timeLimitedExecutor);
    }

    @Override
    public String collectFromProcess(final long pid) throws Exception {
        return collectFromPid(pid, MBEAN_CPU_SAMPLE_SLEEP);
    }

    /**
     * Uses a {@link MBeanCpuUsageReporter} object to obtain information from the Java process id. It binds to the process,
     * sleeps for the provided time to allow sampling, collects CPU data on the process and then returns the results.
     *
     * @param pid        The Java process ID to collect CPU usage info for.
     * @param sampleTime The sleep time that is used for sampling the process.
     * @return The data collected in String form.
     * @throws Exception if any problems with connecting to mbean server / java process cannot be located / thread is interrupted
     *                   or executor service fails to finish
     */
    @Override
    public String collectFromPid(final long pid, final long sampleTime) throws Exception {
        return timeLimitedExecutor.runSync(() -> {
            LOGGER.debug("Generating CPU usage with pid '{}' sleep time '{}'", pid, sampleTime);
            final MBeanCpuUsageReporter cpuUsageReporter = virtualMachineFactory.getMBeanCpuUsageReporter(pid);
            // Need to hit it first to kick off recording, then sleep for the sampling window.
            cpuUsageReporter.report();
            try {
                timeService.sleep(sampleTime);
            } catch (InterruptedException e) {
                LOGGER.warn("Sample sleep period was interrupted, unable to complete cpu sampling.");
                throw e;
            }
            return cpuUsageReporter.report();
        });
    }

    @Override
    public int getPriority() {
        return PRIORITY;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }
}
