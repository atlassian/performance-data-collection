package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.product.DescriptorConverterFactory;
import com.atlassianlabs.perftools.product.Product;
import com.atlassianlabs.perftools.product.descriptors.ProductDescriptor;
import org.gridkit.lab.jvm.attach.JavaProcessDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.atlassianlabs.perftools.helper.MapHelper.asMap;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

@Service
public class ProductDiscovererService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDiscovererService.class);
    private static final String PRODUCT_SERVICE_EVENT_TYPE = "pdc.server.product.ProductDiscovererService";

    private final ApplicationContext context;
    private final DescriptorConverterFactory converterFactory;
    private final VirtualMachineService virtualMachineService;
    private final AnalyticsService analyticsService;

    @Autowired
    public ProductDiscovererService(@NotNull final ApplicationContext context,
                                    @NotNull final DescriptorConverterFactory converterFactory,
                                    @NotNull final VirtualMachineService virtualMachineService,
                                    @NotNull final AnalyticsService analyticsService) {
        this.context = requireNonNull(context);
        this.converterFactory = requireNonNull(converterFactory);
        this.virtualMachineService = requireNonNull(virtualMachineService);
        this.analyticsService = requireNonNull(analyticsService);
    }

    /**
     * @return All product classes that implement a {@link ProductDescriptor} interface.
     */
    private Collection<ProductDescriptor> getProductDescriptors() {
        return context.getBeansOfType(ProductDescriptor.class).values();
    }

    /**
     * Attempt to identify the product from the provided {@link JavaProcessDetails} by iterating
     * through all of the implemented product descriptors, then seeing if we can convert it to a product.
     * If it is identified the constructed {@link Product} object is returned inside an Optional.
     *
     * @param processDetails The Java process as a {@link JavaProcessDetails} object to try to discover the product for.
     * @return A product if it discovered from the process details, otherwise null. For example, if it times out trying to connect, nothing is returned for that product.
     */
    public Optional<Product> getProduct(JavaProcessDetails processDetails) {
        final Optional<Product> product = ofNullable(processDetails)
                .map(d -> getProductDescriptors().stream()
                        .map(converterFactory.getDescriptor(d))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .findAny().orElse(null));
        product.ifPresent(p -> analyticsService.sendAsyncEvent(
                PRODUCT_SERVICE_EVENT_TYPE,
                asMap("product.name", p.getName())));
        return product;
    }

    /**
     * Converts the provided process id into a {@link JavaProcessDetails} object and tries to obtain the product through
     * {@link ProductDiscovererService#getProduct(JavaProcessDetails)}.
     *
     * @param pid The java process id to try to discover the product for.
     * @return An optional of the {@link Product} if discovered, or empty if the process is not discovered.
     */
    public Optional<Product> getProduct(long pid) {
        final JavaProcessDetails javaProcessDetails;
        try {
            javaProcessDetails = virtualMachineService.getProcessDetails(pid);
        } catch (Exception e) { //NOSONAR
            LOGGER.warn(String.format("Process id %s does not appear to be a Java process.", pid), e);
            return Optional.empty();
        }
        return getProduct(javaProcessDetails);
    }

    /**
     * Iterates through all of the runnning Virtual Machines, then attempts to discover the product for each of them.
     *
     * @return A List containing all of the discovered products.
     */
    public List<Product> getAllProducts() {
        final List<Product> productList = virtualMachineService.listRunningVirtualMachines().stream()
                .map(processId -> getProduct(processId.getPID()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());

        if (productList.isEmpty()) {
            analyticsService.sendAsyncEvent(PRODUCT_SERVICE_EVENT_TYPE, asMap("product.name", "None"));
        }
        return productList;
    }
}
