package com.atlassianlabs.perftools.services;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import static org.apache.commons.lang3.SystemUtils.IS_OS_LINUX;
import static org.apache.commons.lang3.SystemUtils.IS_OS_SOLARIS;
import static org.apache.commons.lang3.SystemUtils.IS_OS_UNIX;
import static org.apache.commons.lang3.SystemUtils.IS_OS_WINDOWS;

/**
 * Provides some helper methods for files and file system info. These are often static / IO, placing them here lets
 * us mock out results for unit testing.
 */
@Service
public class FileSystemService {
    /**
     * Converts the provided {@link File} into Java properties, by opening the file and parsing it.
     *
     * @param file The {@link File} to be converted into a Java properties object.
     * @return A {@link Properties} object containing the properties within the file.
     */
    public Properties parsePropertiesFromFile(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);

        Properties properties = new Properties();
        properties.load(inputStream);

        inputStream.close();

        return properties;
    }

    /**
     * Verifies is the provided File is valid (exists and isn't a directory).
     *
     * @param file {@link File} to test.
     * @return true / false
     */
    public boolean isValidFile(File file) {
        return (file.exists() && !file.isDirectory());
    }

    /**
     * Verifies if the provided directory is a directory.
     *
     * @param directory The full path to check if it's a directory.
     * @return true if it's a directory, false if it's not
     */
    public boolean isValidDirectory(String directory) {
        return Files.exists(Paths.get(directory));
    }

    /**
     * "Proxies" to org.apache.commons.lang3.SystemUtils#IS_OS_UNIX
     *
     * @return true if the OS is Unix.
     */
    public boolean isUnix() {
        return IS_OS_UNIX;
    }

    /**
     * "Proxies" to org.apache.commons.lang3.SystemUtils#IS_OS_LINUX
     *
     * @return true if the OS is Linux.
     */
    public boolean isLinux() {
        return IS_OS_LINUX;
    }

    /**
     * "Proxies" to org.apache.commons.lang3.SystemUtils#IS_OS_SOLARIS
     *
     * @return true if the OS is Solaris.
     */
    public boolean isSolaris() {
        return IS_OS_SOLARIS;
    }

    /**
     * "Proxies" to org.apache.commons.lang3.SystemUtils#IS_OS_WINDOWS
     *
     * @return true if the OS is Windows.
     */
    public boolean isWindows() {
        return IS_OS_WINDOWS;
    }

    /**
     * Attempts to get the operation system from System properties.
     *
     * @return The value of the 'os.name' property if present, otherwise "Unable to determine OS" is returned.
     */
    public String getOperatingSystem() {
        return System.getProperty("os.name", "Unable to determine OS.");
    }
}
