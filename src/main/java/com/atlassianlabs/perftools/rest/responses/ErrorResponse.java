package com.atlassianlabs.perftools.rest.responses;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Used by the REST API to output exceptions in JSON.
 */
public class ErrorResponse {
    private final Exception exception;

    public ErrorResponse(final Exception exception) {
        this.exception = exception;
    }

    public String getError() {
        return exception.getLocalizedMessage();
    }

    public StackTraceElement[] getStackTrace() {
        return exception.getStackTrace();
    }

    @Override
    public String toString() {
        return ExceptionUtils.getStackTrace(exception);
    }
}
