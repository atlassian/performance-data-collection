package com.atlassianlabs.perftools.rest.controllers;

import com.atlassianlabs.perftools.rest.responses.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.core.annotation.AnnotatedElementUtils.findMergedAnnotation;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice(basePackages = {"com.atlassianlabs.perftools.rest.controllers"})
public class GlobalRestControllerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalRestControllerAdvice.class);

    /**
     * This applies a global exception handling mechanism for the REST API, so any exceptions that are
     * thrown will be returned in serialised JSON.
     *
     * @param ex The exception to convert to a response entity.
     * @return JSON containing the error and the stack trace of the exception.
     */
    @ExceptionHandler
    ResponseEntity handleException(Exception ex) {
        LOGGER.info("Exception thrown processing request.", ex);
        return new ResponseEntity<>(new ErrorResponse(ex), resolveAnnotatedResponseStatus(ex));
    }

    /**
     * Check if the exception is annotated to have a custom {@link ResponseStatus}. If so, we use that.
     * This is done this way because otherwise the exception status will not be returned due to the default
     * behaviour currently in Spring. This reason why is covered in the {@link ResponseStatus} JavaDocs.
     *
     * @param ex The exception to check to see if it has a status on it.
     * @return The {@link HttpStatus} on the exception, otherwise {@link HttpStatus#INTERNAL_SERVER_ERROR}.
     */
    private HttpStatus resolveAnnotatedResponseStatus(Exception ex) {
        final ResponseStatus annotation = findMergedAnnotation(ex.getClass(), ResponseStatus.class);
        if (annotation != null) {
            return annotation.value();
        }
        return INTERNAL_SERVER_ERROR;
    }
}
