package com.atlassianlabs.perftools.rest.controllers;

import com.atlassianlabs.perftools.product.Product;
import com.atlassianlabs.perftools.rest.exceptions.NotFoundException;
import com.atlassianlabs.perftools.services.ProductDiscovererService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Api(authorizations = {@Authorization(value = "basicAuth")})
@ApiResponses(value = {
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
})
@RestController
@CrossOrigin
@RequestMapping(path = "/rest/product")
public class ProductController {
    private final ProductDiscovererService productDiscovererService;

    public ProductController(final ProductDiscovererService productDiscovererService) {
        this.productDiscovererService = productDiscovererService;
    }

    /**
     * Looks up all running virtual machines on the server and tries to discover if they are a product or not.
     * This will only return processes if they are considered an Atlassian product, and they are discoverable.
     * They will not be discoverable if they are running as a separate user to this Java process.
     *
     * @return A JSON serialisation of the discovered Atlassian {@link Product}(s), or empty if there are no products found.
     */
    @ApiOperation(value = "Identify all Atlassian products running as the same user as performance collector")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "One or more Atlassian products were identified, or an empty list is returned")
    })
    @RequestMapping(path = "/all", method = GET, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Product> getAllProducts() {
        return productDiscovererService.getAllProducts();
    }

    /**
     * Attempts to discover a product by using the passed pid.
     *
     * @param pid The process ID of the running Atlassian product to attempt to discover.
     * @return A JSON serialisation of the discovered Atlassian {@link Product}, or a 404 with an error message if there is no process / it cannot be discovered.
     */
    @ApiOperation(value = "Identify Atlassian product from a process id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully identified Atlassian application from process id"),
            @ApiResponse(code = 404, message = "The provided process id is not an Atlassian application")
    })
    @RequestMapping(path = "/{pid}", method = GET, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public Product getProductByPid(
            @ApiParam(value = "The Java process id of the Atlassian product to get information on", required = true)
            @PathVariable("pid") final long pid) throws NotFoundException {
        final Optional<Product> product = productDiscovererService.getProduct(pid);
        if (product.isPresent()) {
            return product.get();
        } else {
            throw new NotFoundException(String.format("No product was discovered for pid %s.", pid));
        }
    }
}
