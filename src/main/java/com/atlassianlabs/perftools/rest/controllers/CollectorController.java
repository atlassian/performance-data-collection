package com.atlassianlabs.perftools.rest.controllers;


import com.atlassianlabs.perftools.collectors.diskspeed.DiskSpeedCollector;
import com.atlassianlabs.perftools.collectors.results.ComboResult;
import com.atlassianlabs.perftools.collectors.results.CpuUsageResult;
import com.atlassianlabs.perftools.collectors.results.DiskSpeedResult;
import com.atlassianlabs.perftools.collectors.results.ThreadDumpResult;
import com.atlassianlabs.perftools.rest.exceptions.NotFoundException;
import com.atlassianlabs.perftools.rest.exceptions.ServerErrorException;
import com.atlassianlabs.perftools.services.ComboService;
import com.atlassianlabs.perftools.services.CpuUsageService;
import com.atlassianlabs.perftools.services.ThreadDumpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Api(authorizations = {@Authorization(value = "basicAuth")})
@ApiResponses(value = {
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
})
@RestController
@CrossOrigin
@RequestMapping(path = "/rest/collect")
public class CollectorController {
    private static final String DEFAULT_TOTAL_TIMES = "6";
    private static final String DEFAULT_WAIT_TIME = "10000";
    private static final String DEFAULT_DISK_RUNS = "1000";

    private final ThreadDumpService threadDumpService;
    private final DiskSpeedCollector diskSpeedCollector;
    private final CpuUsageService cpuUsageService;
    private final ComboService comboService;

    public CollectorController(final ThreadDumpService threadDumpService, final DiskSpeedCollector diskSpeedCollector,
                               final CpuUsageService cpuUsageService, final ComboService comboService) {
        this.threadDumpService = threadDumpService;
        this.diskSpeedCollector = diskSpeedCollector;
        this.cpuUsageService = cpuUsageService;
        this.comboService = comboService;
    }

    @ApiOperation(value = "Generate a thread dump with the provided Java process id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully generated a thread dump using one of the available collectors"),
            @ApiResponse(code = 404, message = "The collector specified does not exist")
    })
    @RequestMapping(path = "/threaddump/{pid}", method = GET, produces = {APPLICATION_OCTET_STREAM_VALUE, APPLICATION_JSON_VALUE, TEXT_PLAIN_VALUE})
    @ResponseBody
    public ThreadDumpResult generateThreadDump(
            @ApiParam(value = "Java process id to collect a thread dump from", required = true) @PathVariable("pid") final long pid,
            @ApiParam(value = "The name of the thread dump collector to use")
            @RequestParam(value = "collector", required = false, defaultValue = "") final String collector) throws NotFoundException {

        if (collector.isEmpty()) {
            return threadDumpService.collectFromPid(pid);
        } else {
            return threadDumpService.collectFromPid(pid, collector);
        }
    }

    @ApiOperation(value = "Generate Java disk speed results with the provided directory")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully generated Java disk speed results"),
            @ApiResponse(code = 404, message = "The provided directory is not a valid directory")
    })
    @RequestMapping(path = "/diskspeed", method = GET, produces = {APPLICATION_OCTET_STREAM_VALUE, APPLICATION_JSON_VALUE, TEXT_PLAIN_VALUE})
    @ResponseBody
    public DiskSpeedResult generateDiskSpeed(
            @ApiParam(value = "Directory to test Java disk speed in", required = true)
            @RequestParam(value = "dir") final String dir,
            @ApiParam(value = "The total number of times to execute disk speed operations")
            @RequestParam(value = "runs", defaultValue = DEFAULT_DISK_RUNS) final int runs) throws ServerErrorException, NotFoundException {
        try {
            return diskSpeedCollector.collectDiskSpeed(dir, runs);
        } catch (IOException e) {
            throw new ServerErrorException(String.format("Unable to collect disk speed for %s.", dir), e);
        }
    }

    @ApiOperation(value = "Generate multiple thread dumps and cpu usage with the provided Java process id and specified frequency / pause time")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully generated thread dump and/or cpu usage results")})
    @RequestMapping(path = "/combo/{pid}", method = GET, produces = {APPLICATION_OCTET_STREAM_VALUE, APPLICATION_JSON_VALUE, TEXT_PLAIN_VALUE})
    @ResponseBody
    public ComboResult generateThreadDumpsAndCpuUsage(
            @ApiParam(value = "Java process id to collect cpu usage and thread dumps from", required = true) @PathVariable("pid") final long pid,
            @ApiParam("The total number of times to collect thread dump / cpu usage")
            @RequestParam(value = "totalTimes", defaultValue = DEFAULT_TOTAL_TIMES) final int totalTimes,
            @ApiParam("The number of milliseconds to wait between collections")
            @RequestParam(value = "waitTime", defaultValue = DEFAULT_WAIT_TIME) final long waitTime) throws ServerErrorException, InterruptedException {
        return comboService.collectThreadDumpsAndCpuUsage(pid, totalTimes, waitTime);
    }

    @ApiOperation(value = "Generate cpu usage with the provided Java process id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully generated the cpu usage"),
            @ApiResponse(code = 404, message = "The collector specified does not exist")
    })
    @RequestMapping(path = "/cpu/{pid}", method = GET, produces = {APPLICATION_OCTET_STREAM_VALUE, APPLICATION_JSON_VALUE, TEXT_PLAIN_VALUE})
    @ResponseBody
    public CpuUsageResult generateCpuUsage(
            @ApiParam(value = "Java process id to collect cpu usage from", required = true) @PathVariable("pid") final long pid,
            @ApiParam(value = "The name of the cpu usage collector to use")
            @RequestParam(value = "collector", required = false, defaultValue = "") final String collector) throws NotFoundException {

        if (collector.isEmpty()) {
            return cpuUsageService.collectFromPid(pid);
        } else {
            return cpuUsageService.collectFromPid(pid, collector);
        }
    }
}
