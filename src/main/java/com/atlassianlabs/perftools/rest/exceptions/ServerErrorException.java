package com.atlassianlabs.perftools.rest.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * Indicates something bad happening, like the death star getting blown up. Returns a 500.
 */
@ResponseStatus(value = INTERNAL_SERVER_ERROR)
public class ServerErrorException extends Exception {
    public ServerErrorException(String message) {
        super(message);
    }

    public ServerErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
