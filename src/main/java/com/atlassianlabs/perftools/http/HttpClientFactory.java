package com.atlassianlabs.perftools.http;

import com.google.common.collect.ImmutableSet;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.SystemDefaultRoutePlanner;
import org.apache.http.message.BasicHeader;
import org.springframework.stereotype.Component;

import java.net.ProxySelector;

@Component
public class HttpClientFactory {
    /**
     * Provides a CloseableHttpClient that uses a proxy (if configured) with {@literal Accept-Encoding} header set to
     * {@literal gzip, deflate} and uses the provided timeout.
     *
     * @param timeout Timeout in ms for the socket and connection
     * @return A httpclient with the above settings
     */
    public CloseableHttpClient newHttpClient(int timeout) {
        final SocketConfig socketConfig = SocketConfig.custom().setSoTimeout(timeout).build();
        final HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();

        final SystemDefaultRoutePlanner routePlanner = new SystemDefaultRoutePlanner(ProxySelector.getDefault());
        httpClientBuilder.setRoutePlanner(routePlanner);
        httpClientBuilder.setDefaultSocketConfig(socketConfig);
        httpClientBuilder.setDefaultHeaders(
                ImmutableSet.of(new BasicHeader("Accept-Encoding", "gzip, deflate"))
        );

        final RequestConfig.Builder requestConfigBuilder = RequestConfig.custom()
                .setConnectTimeout(timeout)
                .setSocketTimeout(socketConfig.getSoTimeout());
        httpClientBuilder.setDefaultRequestConfig(requestConfigBuilder.build());

        httpClientBuilder.useSystemProperties();

        return httpClientBuilder.build();
    }
}
