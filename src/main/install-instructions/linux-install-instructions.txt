* The toolkit must be run as the same user that the Atlassian application is run as due to Java JDK limitations.
* It requires Java JDK 8 to run.
* Ensure it is put in the appropriate directory for that user that it can access, e.g.: /home/jira
* It has a bundled launch script so can be executed directly, e.g.: ~./data-collector-1.0.jar
* If you want to run it in the background, ~./data-collector-1.0.jar &
* It can be setup as a service as per https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html